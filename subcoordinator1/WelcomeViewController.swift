//
//  WelcomeViewController.swift
//  subcoordinator1
//
//  Created by Jin Hong on 2020-08-18.
//  Copyright © 2020 junk. All rights reserved.
//

import UIKit

class WelcomeActionModel: URLActionModel {
    let isPreLogin: Bool

    required init(url: URL) throws {
        isPreLogin = false
    }

    func viewController() -> UIViewController {
        WelcomeViewController()
    }
}
class WelcomeViewController: ActionViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .purple
        addText("WELCOME")
        addButton(title: "NEXT", action: #selector(didTapNextButton))
    }

    @objc private func didTapNextButton(_ sender: UIButton) {
        URL(string: "https://app/verifyPhone").map(performAction)
    }
}

class VerifyPhoneActionModel: URLActionModel {
    let isPreLogin: Bool

    required init(url: URL) throws {
        isPreLogin = false
    }

    func viewController() -> UIViewController {
        VerifyPhoneViewController()
    }
}
class VerifyPhoneViewController: ActionViewController {

    override var actionFlowPresentations: [ActionViewController.Type]? { [VerifyPhoneCodeViewController.self] }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemGray4
        addText("VERIFYPHONE")
        addButton(title: "DONE", action: #selector(didTapDoneButton))
        addButton(title: "CODE", action: #selector(didTapCodeButton))
    }

    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        guard
            let verifyPhoneCodeViewController = presentedViewController as? VerifyPhoneCodeViewController
        else { return super.dismiss(animated: flag, completion: completion) }
        switch verifyPhoneCodeViewController.actionFlowData?[VerifyPhoneCodeViewController.skipped] {
        case true: presentingViewController?.dismiss(animated: flag, completion: completion)
        default: super.dismiss(animated: flag, completion: completion)
        }
    }

    @objc private func didTapDoneButton(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

    @objc private func didTapCodeButton(_ sender: UIButton) {
        URL(string: "https://app/verifyPhoneCode").map(performAction)
    }
}

class VerifyPhoneCodeActionModel: URLActionModel {
    let isPreLogin: Bool

    required init(url: URL) throws {
        isPreLogin = false
    }

    func viewController() -> UIViewController {
        VerifyPhoneCodeViewController()
    }
}
class VerifyPhoneCodeViewController: ActionViewController {
    static let skipped: ActionFlowDataKey = .createKey("skipped")
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemTeal
        addText("VERIFYPHONECODE")
        addButton(title: "DONE", action: #selector(didTapDoneButton))
        addButton(title: "SKIP", action: #selector(didTapSkipButton))
    }

    @objc private func didTapDoneButton(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

    @objc private func didTapSkipButton(_ sender: UIButton) {
        actionFlowData = [Self.skipped: true]
        dismiss(animated: true, completion: nil)
    }
}
