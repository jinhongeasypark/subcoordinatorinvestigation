import UIKit

struct ActionSuccessEvent: Event {
    let name: String
    let event: ActionEvent
    let properties: [String : Any]?

    init(_ event: ActionEvent) {
        self.name = "Action Success Event"
        self.event = event
        self.properties = ["Action": event.action ?? "nil action"]
    }
}
struct ActionFailedEvent: Event {
    let name: String
    let error: ActionModelError
    let properties: [String : Any]?

    init(_ error: ActionModelError) {
        self.name = "Action Failed Event"
        self.error = error
        self.properties = ["Summary": error.summary, "Description": error.description]
    }
}

protocol Coordinator {
    func handleActionModel(_ actionModel: ActionModel)
}

class SomeCoordinator: Coordinator, EventSubscriber {
    private enum State {
        case uninitialized, initialised(ActionModel)
    }

    static let shared: Coordinator = SomeCoordinator()

    private let eventBus: EventBus
    private var onMainActionModel: ActionModel?
    private var state: State!
    private weak var sceneDelegate: SceneDelegate!

    private init(eventBus: EventBus = EventBusSingleton.shared) {
        self.eventBus = eventBus
        self.onMainActionModel = nil
        self.state = nil
        self.sceneDelegate = nil
        self.eventBus.subscribe(self, to: ActionEvent.self, onEvent: onActionEvent)
        self.eventBus.subscribe(self, to: ActionModelEvent.self, onEvent: onActionModelEvent)
        self.eventBus.subscribe(self, to: MainViewPresentedEvent.self, onEvent: onMainViewPresentedEvent)
    }

    deinit {
        sceneDelegate = nil
        eventBus.unsubscribe(self, from: ActionEvent.self)
        eventBus.unsubscribe(self, from: ActionModelEvent.self)
        eventBus.unsubscribe(self, from: MainViewPresentedEvent.self)
    }

    private func onActionEvent(_ event: ActionEvent) {
        guard let actionUrl = event.action else {
            return
        }
        do {
            let actionModel = try actionUrl.toActionModel()
            handleActionModel(actionModel)
        } catch let error as ActionModelError {
            print(error)
        } catch let error {
            print(error)
        }
    }

    private func onActionModelEvent(_ event: ActionModelEvent) {
        handleActionModel(event.actionModel)
    }

    private func onMainViewPresentedEvent(_ event: MainViewPresentedEvent) {
        onMainActionModel.map(handleActionModel)
        onMainActionModel = nil
    }

    func start(with appDelegate: SceneDelegate) {
        self.sceneDelegate = appDelegate
        self.handleActionModel(SplashActionModel())
    }

    func handleActionModel(_ actionModel: ActionModel) {
        switch state {
        case .uninitialized:
            guard actionModel.isPreLogin || actionModel is MainActionModel
                else { return onMainActionModel = actionModel }
            replaceRootViewController(from: actionModel)
        case .initialised:
            if let topActionViewController = sceneDelegate.window?.rootViewController?.topViewController as? ActionViewController,
               topActionViewController.performsCustomNavigation(of: actionModel) {
                topActionViewController.performCustomNavigation(of: actionModel)
            } else if let actionViewController = viewControllerHandling(actionModel: actionModel) {
                actionViewController.dismiss(animated: true) {
                    actionViewController.performCustomNavigation(of: actionModel)
                }
            } else {
                presentViewController(from: actionModel)
            }
        case .none: setRootViewController(from: actionModel)
        }
    }

    private func setRootViewController(from actionModel: ActionModel) {
        sceneDelegate.window?.rootViewController = actionModel.viewController()
        sceneDelegate.window?.makeKeyAndVisible()
        switch actionModel {
        case is SplashActionModel: state = .uninitialized
        default: state = .initialised(actionModel)
        }
    }

    private func replaceRootViewController(from actionModel: ActionModel) {
        sceneDelegate.window?.rootViewController = actionModel.viewController()
        sceneDelegate.window?.makeKeyAndVisible()
        sceneDelegate.window.map {
            UIView.transition(with: $0, duration: 0.25, options: .transitionCrossDissolve, animations: nil, completion: nil)
        }
        switch actionModel {
        case is SplashActionModel: state = .uninitialized
        default: state = .initialised(actionModel)
        }
    }

    private func presentViewController(from actionModel: ActionModel) {
        sceneDelegate.window?.rootViewController.map { root in
            let visible = root.topViewController
            let presenting = actionModel.viewController()
            presenting.modalPresentationStyle = .overFullScreen
            presenting.modalTransitionStyle = .crossDissolve
            visible.present(presenting, animated: true)
        }
    }

    private func viewControllerHandling(actionModel: ActionModel) -> ActionViewController? {
        var viewController = sceneDelegate.window?.rootViewController?.topViewController
        while let currentViewController = viewController {
            print("CHECKING:", currentViewController.description)
            if let actionViewController = currentViewController as? ActionViewController,
                actionViewController.performsCustomNavigation(of: actionModel) {
                print("RETURNING:", actionViewController.description)
                return actionViewController
            }
            viewController = currentViewController.presentingViewController
        }
        return nil
    }
}

class RootCoordinator: Coordinator, EventSubscriber {

    private enum State {
        case splash, preLogin, main, presented
    }

//    static let shared: Coordinator = RootCoordinator()

    private let eventBus: EventBus
    private var onMainActionModel: ActionModel?
    private var state: State!
    private weak var sceneDelegate: SceneDelegate!

    private init(eventBus: EventBus = EventBusSingleton.shared) {
        self.eventBus = eventBus
        self.onMainActionModel = nil
        self.state = nil
        self.sceneDelegate = nil
        self.eventBus.subscribe(self, to: ActionEvent.self, onEvent: onActionEvent)
        self.eventBus.subscribe(self, to: ActionModelEvent.self, onEvent: onActionModelEvent)
        self.eventBus.subscribe(self, to: MainViewPresentedEvent.self, onEvent: onMainViewPresentedEvent)
    }

    deinit {
        sceneDelegate = nil
        eventBus.unsubscribe(self, from: ActionEvent.self)
        eventBus.unsubscribe(self, from: ActionModelEvent.self)
        eventBus.unsubscribe(self, from: MainViewPresentedEvent.self)
    }

    private func onActionEvent(_ event: ActionEvent) {
        guard let actionUrl = event.action else {
            return
        }
        do {
            let actionModel = try actionUrl.toActionModel()
            handleActionModel(actionModel)
        } catch let error as ActionModelError {
            print(error)
        } catch let error {
            print(error)
        }
    }

    private func onActionModelEvent(_ event: ActionModelEvent) {
        handleActionModel(event.actionModel)
    }

    private func onMainViewPresentedEvent(_ event: MainViewPresentedEvent) {
        onMainActionModel.map(handleActionModel)
        onMainActionModel = nil
    }

    func start(with appDelegate: SceneDelegate) {
        self.sceneDelegate = appDelegate
        self.handleActionModel(SplashActionModel())
    }

    func handleActionModel(_ actionModel: ActionModel) {
        switch state {
        case .splash, .preLogin:
            guard !topViewControllerDidHandle(actionModel: actionModel)
                else { return }
            guard actionModel.isPreLogin || actionModel is MainActionModel
                else { return onMainActionModel = actionModel }
            replaceRootViewController(from: actionModel)
        case .main, .presented:
            guard !topViewControllerDidHandle(actionModel: actionModel)
                else { return }
            actionModel.isPreLogin || actionModel is MainActionModel ?
                replaceRootViewController(from: actionModel) :
                presentViewController(from: actionModel)
        case nil:
            // should only happen once on Coodinator.start()
            setRootViewController(from: actionModel)
        }
    }

    private func topViewControllerDidHandle(actionModel: ActionModel) -> Bool {
        guard
            let actionViewController = sceneDelegate.window?.rootViewController?.topViewController as? ActionViewController,
            actionViewController.performsCustomNavigation(of: actionModel)
            else { return false }
        actionViewController.performCustomNavigation(of: actionModel)
        state = (state == .main && actionViewController.presentedViewController != nil) ? .presented : state
        return true
    }

    private func setRootViewController(from actionModel: ActionModel) {
        sceneDelegate.window?.rootViewController = actionModel.viewController()
        sceneDelegate.window?.makeKeyAndVisible()
        switch actionModel {
        case is SplashActionModel: state = .splash
        case is MainActionModel: state = .main
        default: state = actionModel.isPreLogin ? .preLogin : state
        }
    }

    private func replaceRootViewController(from actionModel: ActionModel) {
        sceneDelegate.window?.rootViewController = actionModel.viewController()
        sceneDelegate.window?.makeKeyAndVisible()
        sceneDelegate.window.map {
            UIView.transition(with: $0, duration: 0.25, options: .transitionCrossDissolve, animations: nil, completion: nil)
        }
        switch actionModel {
        case is MainActionModel: state = .main
        default: state = actionModel.isPreLogin ? .preLogin : state
        }
    }

    private func presentViewController(from actionModel: ActionModel) {
        sceneDelegate.window?.rootViewController.map { root in
            let visible = root.topViewController
            let presenting = actionModel.viewController()
            presenting.modalPresentationStyle = .overFullScreen
            presenting.modalTransitionStyle = .crossDissolve
            visible.present(presenting, animated: true)
            state = .presented
        }
    }
}

enum ActionModelError: Error, CustomStringConvertible {
    case missingActionModel(URL)
    case nilActionOnEvent
    case convertingToActionModel(URL)
    case couldNotInitialize(Error)
    case configurationFailed(Error)

    var summary: String {
        switch self {
        case .missingActionModel: return "missing action model"
        case .nilActionOnEvent: return "nil action on event"
        case .convertingToActionModel: return "converting to action model"
        case .couldNotInitialize: return "could not initialize"
        case .configurationFailed: return "configuration failed"
        }
    }

    var description: String {
        switch self {
        case .missingActionModel(let url): return "ActionModelError - missing action model: \(url.lastPathComponent.toPascalCase)ActionModel does not exist"
        case .nilActionOnEvent: return "ActionModelError - nil action on event: ActionEvent triggered with nil action property"
        case .convertingToActionModel(let url): return "ActionModelError - converting to action model: somehow unable to convert URL \(url) to ActionModel"
        case .couldNotInitialize(let error): return "ActionModelError - could not initialize: \(error.localizedDescription)"
        case .configurationFailed(let error): return "ActionModelError - configuration failed: \(error.localizedDescription)"
        }
    }
}

extension URL {
    func toActionModel() throws -> ActionModel {
        guard
            let actionModelClass = NSClassFromString("subcoordinator1.\(lastPathComponent.toPascalCase)ActionModel") as? URLInitializable.Type
            else { throw ActionModelError.missingActionModel(self) }
        do {
            guard
                let actionModel = try actionModelClass.init(url: self) as? ActionModel
                else { throw ActionModelError.convertingToActionModel(self) }
            return actionModel
        } catch let error {
            throw ActionModelError.couldNotInitialize(error)
        }
    }

    var queriesDictionary: [String: Any] {
        var dict: [String: Any] = [:]
        URLComponents(url: self, resolvingAgainstBaseURL: false)?.queryItems?.forEach { item in
            item.value.map { dict[item.name] = $0 }
        }
        return dict
    }
}

private extension String {
    /// camelCase to PascalCase
    var toPascalCase: String {
        return prefix(1).uppercased() + dropFirst()
    }
}

private extension UIViewController {
    var topViewController: UIViewController {
        if let navigationController = self as? UINavigationController,
            let visibleViewController = navigationController.visibleViewController {
            return visibleViewController.topViewController
        }
        return presentedViewController?.topViewController ?? self
    }
}
