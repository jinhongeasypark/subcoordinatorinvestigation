protocol Event {}

protocol EventSubscriber: class {
}

protocol EventPublisher {
    func publish(_ event: Event)
}
