//
//  PaymentMethodsViewController.swift
//  subcoordinator1
//
//  Created by Jin Hong on 2020-08-12.
//  Copyright © 2020 junk. All rights reserved.
//

import UIKit

class PaymentMethodsActionModel: URLActionModel {

    let isPreLogin: Bool

    required init(url: URL) throws {
        isPreLogin = false
    }

    func viewController() -> UIViewController {
        PaymentMethodsViewController()
    }
}
class PaymentMethodsViewController: ActionViewController {

    override var actionFlowPresentations: [ActionViewController.Type]? { [AfterpayViewController.self] }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .cyan
        addText("PAYMENTMETHODS")
        addButton(title: "AFTERPAY", action: #selector(didTapAfterpayButton))
    }

    override func performsCustomNavigation(of actionModel: ActionModel) -> Bool {
        return actionModel is AfterpayActionModel
    }

    override func performCustomNavigation(of actionModel: ActionModel) {
        let alert = UIAlertController(title: "AFTERPAY", message: nil, preferredStyle: .alert)
        let goAction = UIAlertAction(title: "GO", style: .default) { _ in
            self.present(actionModel, animated: true)
        }
        alert.addAction(goAction)
        present(alert, animated: true)
    }

    @objc private func didTapAfterpayButton(_ sender: UIButton) {
        URL(string: "https://app/afterpay").map(performAction)
    }

    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        guard let afterpayViewController = presentedViewController as? AfterpayViewController else {
            return super.dismiss(animated: flag, completion: completion)
        }
        switch afterpayViewController.actionFlowData?[AfterpayViewController.done] {
        case true: presentingViewController?.dismiss(animated: flag, completion: completion)
        default:
            super.dismiss(animated: flag) { [unowned self] in
                let alert = UIAlertController(title: "WOW", message: "FAIL", preferredStyle: .alert)
                let okDismissAction = UIAlertAction(title: "OK", style: .default) { _ in
                    self.presentingViewController?.dismiss(animated: flag, completion: completion)
                }
                let tryAgainAction = UIAlertAction(title: "TRY AGAIN", style: .default, handler: nil)
                alert.addAction(okDismissAction)
                alert.addAction(tryAgainAction)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
}

class AfterpayActionModel: URLActionModel {
    let isPreLogin: Bool

    required init(url: URL) throws {
        isPreLogin = false
    }

    func viewController() -> UIViewController {
        AfterpayViewController()
    }
}
class AfterpayViewController: ActionViewController {
    static let done: ActionFlowDataKey = .createKey("done")

    override var actionFlowPresentations: [ActionViewController.Type] { [AfterpayDoneViewController.self] }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .green
        addText("AFTERPAY")
        addButton(title: "POLICY", action: #selector(didTapPolicyButton))
        addButton(title: "DONE", action: #selector(didTapDoneButton))
    }

    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        defer { super.dismiss(animated: flag, completion: completion) }
        guard let afterpayDoneViewController = presentedViewController as? AfterpayDoneViewController else {
            return
        }
        actionFlowData = [Self.done: afterpayDoneViewController.actionFlowData?[AfterpayDoneViewController.done] ?? false]
    }

    @objc private func didTapPolicyButton(_ sender: UIButton) {
        URL(string: "https://app/policy").map(performAction)
    }

    @objc private func didTapDoneButton(_ sender: UIButton) {
        URL(string: "https://app/afterpayDone").map(performAction)
    }
}

class AfterpayDoneActionModel: URLActionModel {
    let isPreLogin: Bool

    required init(url: URL) throws {
        isPreLogin = false
    }

    func viewController() -> UIViewController {
        AfterpayDoneViewController()
    }
}
class AfterpayDoneViewController: ActionViewController {
    static let done: ActionFlowDataKey = .createKey("done")

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemTeal
        addText("AFTERPAYDONE")
        addButton(title: "POLICY", action: #selector(didTapPolicyButton))
        addButton(title: "MENU", action: #selector(didTapMenuButton))
        addButton(title: "DONE", action: #selector(didTapDoneButton))
        addButton(title: "EXIT", action: #selector(didTapExitButton))
    }

    @objc private func didTapPolicyButton(_ sender: UIButton) {
        URL(string: "https://app/policy").map(performAction)
    }

    @objc private func didTapMenuButton(_ sender: UIButton) {
        URL(string: "https://app/menu").map(performAction)
    }

    @objc private func didTapDoneButton(_ sender: UIButton) {
        actionFlowData = [Self.done: true]
        dismiss(animated: true)
    }

    @objc private func didTapExitButton(_ sender: UIButton) {
        dismiss(animated: true)
    }
}

class PolicyActionModel: URLActionModel {
    let isPreLogin: Bool

    required init(url: URL) throws {
        isPreLogin = false
    }

    func viewController() -> UIViewController {
        PolicyViewController()
    }
}
class PolicyViewController: ActionViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .red
        addText("POLICY")
        addButton(title: "DONE", action: #selector(didTapDoneButton))
    }

    @objc private func didTapDoneButton(_ sender: UIButton) {
        dismiss(animated: true)
    }
}
