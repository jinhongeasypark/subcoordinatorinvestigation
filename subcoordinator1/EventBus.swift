protocol EventBus: EventPublisher {
    func subscribe<T: Event>(_ subscriber: EventSubscriber, to Event: T.Type, onEvent: @escaping (T) -> Void)
    func unsubscribe<T: Event>(_ subscriber: EventSubscriber, from Event: T.Type)
    func unsubscribeFromAll(_ subscriber: EventSubscriber)
}

private class Subscription {
    let subscriber: EventSubscriber
    let onEvent: (Event) -> Void
    init(subscriber: EventSubscriber, onEvent: @escaping (Event) -> Void) {
        self.subscriber = subscriber
        self.onEvent = onEvent
    }
}

class EventBusSingleton: EventBus {

    static let shared: EventBus = EventBusSingleton()

    private typealias EventIdentifier = String
    private var eventSubscriptions: [EventIdentifier: [Subscription]] = [:]

    private init() {
    }

    func publish(_ event: Event) {
        let eventIdentifier = String(describing: type(of: event))
        eventSubscriptions[eventIdentifier]?.forEach { subscription in
            subscription.onEvent(event)
        }
    }

    func subscribe<T: Event>(_ subscriber: EventSubscriber, to event: T.Type, onEvent: @escaping (T) -> Void) {
        let eventIdentifier = String(describing: event)
        let existingSubscriptions = eventSubscriptions[eventIdentifier]
        let subscription = Subscription(subscriber: subscriber) { [onEvent] event in
            guard let concreteEvent = event as? T else { return }
            onEvent(concreteEvent)
        }
        eventSubscriptions[eventIdentifier] = existingSubscriptions.map { $0 + [subscription] } ?? [subscription]
    }

    func unsubscribe<T: Event>(_ subscriber: EventSubscriber, from event: T.Type) {
        let eventIdentifier = String(describing: event)
        guard var existingSubscription = eventSubscriptions[eventIdentifier] else { return }
        existingSubscription.removeAll { subscription -> Bool in
            return subscription.subscriber === subscriber
        }
        eventSubscriptions[eventIdentifier] = existingSubscription.isEmpty ? nil : existingSubscription
    }

    func unsubscribeFromAll(_ subscriber: EventSubscriber) {
        eventSubscriptions.forEach { (eventIdentifier, subscriptions) in
            let filteredSubscriptions = subscriptions.filter { $0.subscriber !== subscriber }
            let newSubscriptions = filteredSubscriptions.isEmpty ? nil : filteredSubscriptions
            self.eventSubscriptions[eventIdentifier] = newSubscriptions
        }
    }
}
