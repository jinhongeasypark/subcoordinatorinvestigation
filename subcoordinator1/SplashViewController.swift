//
//  SplashViewController.swift
//  subcoordinator1
//
//  Created by Jin Hong on 2020-08-10.
//  Copyright © 2020 junk. All rights reserved.
//

import UIKit

class SplashActionModel: ActionModel {
    func viewController() -> UIViewController {
        SplashViewController()
    }
}
class SplashViewController: ActionViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .blue
        addText("SPLASH")
    }
}
