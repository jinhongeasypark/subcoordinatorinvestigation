import UIKit

struct ActionEvent: Event { let action: URL? }
struct ActionModelEvent: Event { let actionModel: ActionModel }

/**
 Dictionary key for action flow data dictionary. Keys must be created
 using the `createKey` function and be stored as a static variable in
 the view controller.
 ```
 class SomeActionViewController: ActionViewController {
     static let theKey: ActionFlowDataKey = .createKey("theKey")
 }
 ```
 */
typealias ActionFlowDataKey = String

extension ActionFlowDataKey {
    static func createKey(callerName: String = #function, _ key: String) -> String {
        return "\(callerName).\(key)"
    }
}

protocol ActionFlowChain: class where Self: ActionViewController {
    var actionFlowData: Dictionary<ActionFlowDataKey, Bool>? { get set }
    var actionFlowPresentations: [ActionViewController.Type]? { get }
}

class ActionViewController: UIViewController, ActionFlowChain {
    private let eventBus: EventBus = EventBusSingleton.shared
    private let coordinator: Coordinator  = SomeCoordinator.shared

    var actionFlowData: Dictionary<ActionFlowDataKey, Bool>?
    var actionFlowPresentations: [ActionViewController.Type]? { nil }

    private var buttonStackView: UIStackView!
    private var doubleTapGestureRecognizer: UITapGestureRecognizer!

    final func performAction(_ action: URL) {
        eventBus.publish(ActionEvent(action: action))
    }

    final func performActionModel(_ actionModel: ActionModel) {
        coordinator.handleActionModel(actionModel)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        buttonStackView = UIStackView()
        buttonStackView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(buttonStackView)
        buttonStackView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        buttonStackView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        buttonStackView.axis = .vertical
        buttonStackView.alignment = .center
        buttonStackView.distribution = .fillEqually

        doubleTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didDoubleTapView))
        doubleTapGestureRecognizer.numberOfTapsRequired = 2
        view.addGestureRecognizer(doubleTapGestureRecognizer)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print(description, "will appear")
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print(description, "did appear")
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print(description, "will disappear")
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print(description, "did disappear")
    }

    @objc private func didDoubleTapView(_ sender: UITapGestureRecognizer) {
        URL(string: "https://app/main").map(performAction)
    }

    /**
     Override to let the `RootCoordinator` know that this view controller
     will handle the navigation to the argument `actionModel`'s view.
     ```
     override func performsCustomNavigation(of actionModel: ActionModel) -> Bool {
         return actionModel is ConcreteActionModel || actionModel is AnotherActionModel
     }
     ```
     - Parameter actionModel: check and return `true` based on type (default returns `false`)
     */
    func performsCustomNavigation(of actionModel: ActionModel) -> Bool {
        return false
    }

    /**
     Override to handle the navigation to the `actionModel`.
     - Important: Must not present asynchronously.
     - Parameter actionModel: present the `actionModel.viewController()` contextually.
     */
    func performCustomNavigation(of actionModel: ActionModel) {

    }

    /**
     Default presentation of `actionModel.viewController()`. Over full screen and cross dissolve.
     - Parameter actionModel: The action model to display over the current view controller’s content.
     - Parameter flag: Pass `true` to animate the presentation; otherwise, pass `false`.
     - Parameter completion: The block to execute after the presentation finishes. This block has no return value and takes no parameters. You may specify `nil` for this parameter.
     */
    final func present(_ actionModel: ActionModel, animated flag: Bool, completion: (() -> Void)? = nil) {
        let viewController = actionModel.viewController()
        viewController.modalPresentationStyle = .overFullScreen
        viewController.modalTransitionStyle = .crossDissolve
        present(viewController, animated: flag, completion: completion)
    }

    /**
     Dismisses the view controller that was presented modally by the view controller except when `self.actionFlowDelegate != nil` then the dismiss is delegated to the `actionFlowDelegate`.
     - Parameter flag: Pass `true` to animate the transition.
     - Parameter completion: The block to execute after the view controller is dismissed. This block has no return value and takes no parameters. You may specify `nil` for this parameter.
     */
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        guard let presentingActionFlowChainViewController = presentingViewController as? ActionFlowChain,
              presentingActionFlowChainViewController.actionFlowPresentations?.contains(where: { $0 == type(of: self) }) == true else {
            return super.dismiss(animated: flag, completion: completion)
        }
        presentingActionFlowChainViewController.dismiss(animated: flag, completion: completion)
    }
}

extension ActionViewController {
    func addText(_ text: String) {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = text
        label.sizeToFit()

        view.addSubview(label)
        view.safeAreaLayoutGuide.topAnchor.constraint(equalTo: label.topAnchor).isActive = true
        view.centerXAnchor.constraint(equalTo: label.centerXAnchor).isActive = true
    }

    func addButton(title: String, action: Selector) {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(.black, for: .normal)
        button.setTitle(title, for: .normal)
        button.sizeToFit()
        button.addTarget(self, action: action, for: .touchUpInside)
        buttonStackView.addArrangedSubview(button)
    }

    override var description: String { String(describing: type(of: self)) }
}
