//
//  MenuViewController.swift
//  subcoordinator1
//
//  Created by Jin Hong on 2020-09-25.
//  Copyright © 2020 junk. All rights reserved.
//

import UIKit

class MenuActionModel: URLActionModel {

    let isPreLogin: Bool

    required init(url: URL) throws {
        isPreLogin = false
    }

    func viewController() -> UIViewController {
        MenuViewController()
    }
}
class MenuViewController: ActionViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemPink
        addText("MENU")
        addButton(title: "PAYMENTMETHODS", action: #selector(didTapPaymentMethodsButton))
        addButton(title: "WELCOME", action: #selector(didTapWelcomeButton))
    }

    @objc private func didTapPaymentMethodsButton(_ sender: UIButton) {
        URL(string: "https://app/paymentMethods").map(performAction)
    }

    @objc private func didTapWelcomeButton(_ sender: UIButton) {
        URL(string: "https://app/welcome").map(performAction)
    }
}
