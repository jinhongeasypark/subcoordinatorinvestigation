//
//  MainViewController.swift
//  subcoordinator1
//
//  Created by Jin Hong on 2020-08-10.
//  Copyright © 2020 junk. All rights reserved.
//

import UIKit

struct MainViewPresentedEvent: Event {}

class MainActionModel: URLActionModel {

    let isPreLogin: Bool

    required init(url: URL) throws {
        isPreLogin = false
    }

    func viewController() -> UIViewController {
        MainViewController()
    }
}
class MainViewController: ActionViewController {
    private let eventBus: EventBus = EventBusSingleton.shared

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .gray
        addText("MAIN")
        addButton(title: "MENU", action: #selector(didTapMenuButton))
        addButton(title: "PAYMENTMETHODS", action: #selector(didTapPaymentMethodsButton))
        addButton(title: "WELCOME", action: #selector(didTapWelcomeButton))
        addButton(title: "VERIFY", action: #selector(didTapVerifyButton))
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        eventBus.publish(MainViewPresentedEvent())
    }

    override func performsCustomNavigation(of actionModel: ActionModel) -> Bool {
        return actionModel is MenuActionModel || actionModel is PaymentMethodsActionModel
    }

    override func performCustomNavigation(of actionModel: ActionModel) {
        switch actionModel {
        case is MenuActionModel: present(actionModel, animated: true)
        case is PaymentMethodsActionModel: present(actionModel, animated: true)
        default: break
        }
    }

    @objc private func didTapMenuButton(_ sender: UIButton) {
        URL(string: "https://app/menu").map(performAction)
    }

    @objc private func didTapPaymentMethodsButton(_ sender: UIButton) {
        URL(string: "https://app/paymentMethods").map(performAction)
    }

    @objc private func didTapWelcomeButton(_ sender: UIButton) {
        URL(string: "https://app/welcome").map(performAction)
    }

    @objc private func didTapVerifyButton(_ sender: UIButton) {
        URL(string: "https://app/verifyPhone").map(performAction)
    }
}
