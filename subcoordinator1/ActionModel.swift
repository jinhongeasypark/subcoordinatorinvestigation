import UIKit

protocol ActionModel: class, MirrorStringConvertible {
    var isPreLogin: Bool { get }
    func viewController() -> UIViewController
}

extension ActionModel {
    var isPreLogin: Bool { return false }
}

struct URLInitializationError: Error {}

protocol URLInitializable {
    init(url: URL) throws
}

protocol URLActionModel: ActionModel, URLInitializable {}

//extension URLActionModel {
//    static func isPreLogin(_ url: URL, fallback: Bool = false) -> Bool {
//        return url.queriesDictionary.require("isPreLogin", fallback: fallback)
//    }
//}

protocol MirrorStringConvertible: CustomStringConvertible {}
extension MirrorStringConvertible {
    var description: String {
        let mirror = Mirror(reflecting: self)
        let children = mirror.children.reduce("") { (result, arg1) -> String in
            let (label, value) = arg1
            let description = label.map { $0 + ": " + String(describing: value) } ?? "unlabeled: " + String(describing: value)
            return result + "\n\t" + description + ","
        }
        return "\(mirror.subjectType)" + (children.isEmpty ? "" : " { \(children)\n}")
    }
}
